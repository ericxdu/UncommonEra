Naturally, stories set in Uncommon Era may contradict other stories set 
in Uncommon Era since there could be many authors, much experimentation, 
and rampant speculation. As such, anything that isn't absolutely written 
in stone about Uncommon Era will--for the time being--be sorted into the
apocryphal category.

This is NOT to say any self-coherent story, set of stories, or 
in-universe histories aren't canonical. Rather, this repository is being 
designed to be as un-constraining as possible and allow derivative works 
the most freedom to describe a self-consistent timeline.
