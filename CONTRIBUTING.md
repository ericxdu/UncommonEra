Uncommon Era is a free culture multi-media franchise requiring 
contributions from various disciplines including:

  - concept art
  - short stories
  - RPG sourcebooks
  - political maps
  - encyclopedic articles
  - timelines
  - historical texts
  - refinement to the general concepts

All contributions should be submitted with a copyright license 
compatible with Creative Commons license CC-BY-SA 4.0, a copy of which 
is included in the file LICENSE. To view a copy of this license, visit 
http://creativecommons.org/licenses/by-sa/4.0/.

To find a license that is compatible with CC-BY-SA, you may use the 
following resources.

https://wiki.creativecommons.org/wiki/Wiki/cc_license_compatibility

https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses/

