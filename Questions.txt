This is a highly undeveloped science-fiction universe and many questions 
have yet to be answered in detail.

What is the mysterious singularity and how does it give Humanity the 
ability to create technological marvels as early as the first millenium? 
Is it electricity? Atomic power? Does it come from crystals? Subspace 
objects? Alternate dimensions? Quantum machines?

Does the technology lean toward magic? or does it stay somewhat grounded 
in sci-fi?
