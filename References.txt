Edgar Rice Burroughs, Jules Verne, and H. G. Wells serve as inspiration 
for much of the ideas building Uncommon Era. Burroughs' works especially
about Mars, Venus, and Jupiter have the hint of mysticism needed to make
science-fiction technology mesh with first-millenium society and beyond.

The Skeleton Men of Jupiter, ERB - 
http://www.erblist.com/erblist/jcmskeletonsum.html - for an account of 
FTL travel, humanoid aliens, and a habitable Jupiter

Pirates of Venus, ERB - http://www.erblist.com/erblist/ven1sum.html - 
for a summary of the landscape, peoples, and technology of a speculated 
Venus

Gods of Mars ERB C.H.A.S.E.R. - http://www.erbzine.com/mag4/0423.html - 
for some pictures of a speculated Mars

Gods of Mars, the sequel to A Princess of Mars, was an in-depth 
description of the disparate peoples, religions, and technologies of 
Mars - 
http://www.loyalbooks.com/book/the-gods-of-mars-by-edgar-rice-burroughs
