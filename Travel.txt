Peoples tend to use indigenous animals suitable for riding; or rely 
anti-gravity flying machines. There could be exceptions, i.e. powered 
wheeled vehicles.

For space travel, the cost and effort required to escape a planet's 
gravitational field is exceptionally low. Characters could use 
gunpowder, antigravity paint, passing comets, and all manner of devices 
to leave the ground for space travel.

One method uses an invisible ray that negates or reverses the effects of 
gravity combined with cosmic rays and exploiting gravity effects to 
reach superlight speeds; a matter of dozens of days between solar 
planets. Ships are equipped with another invisible ray that generates 
atmosphere, bound to the ship or to individuals by some sort of 
mechanism so they can breathe.

Spacecraft and aircraft in Uncommon Era resemble watercraft; 
boats that float in the air and fly through space.
